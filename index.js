function filterBy(array, string){
let filteredArr = [];
 for (let i = 0; i < array.length; i++){
    let data = array[i];
    if(typeof data !== string){
        filteredArr.push(data);
    }
 }
 return filteredArr;
}

let result = (filterBy(['hello', 'world', 23, '23', null], "string"))
console.log(result)
